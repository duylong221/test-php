<?php
    function tongMax($arr){
        for($i=0; $i<count($arr); $i++){
            if($i == 0) $max1 = $arr[$i];
            else if($i == 1) $max2 = $arr[$i];
            else if($arr[$i]>=$max1){
                $max2 = $max1;
                $max1 = $arr[$i];
            }
            else if($max1 > $arr[$i] && $arr[$i] > $max2){
                $max2 = $arr[$i];
            }
        }
        return ($max1 + $max2);
    }

    $arr = [0, 100, -4, 8, 143, 5, 99, 100];
    echo tongMax($arr);
?>